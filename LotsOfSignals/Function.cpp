#include "pch.h"
#include "Methods.h"
#include "Generation.h"
#include "LotsOfSignalsDlg.h"
#include <vector>

vector<int> CLotsOfSignalsDlg::find_max_n(int n, Signal& sig, double clearance)
{
	if (sig.signal.empty()) return {};
	vector<double> abs_values(sig.signal.size());
	for (int i = 0; i < abs_values.size(); i++)
	{
		abs_values[i] = abs(sig.signal[i]);
	}

	vector <int> max_inds;
	for (int max_idx = 0; max_idx < n; max_idx++)
	{
		int max_ind = 0;
		for (int i = 1; i < sig.signal.size() - 1; i++)
		{
			if (abs_values[i - 1] < abs_values[i] && abs_values[i + 1] < abs_values[i])
			{
				int peak_index = i;
				bool unique_peak = true;
				for (int j = 0; j < max_inds.size(); j++)
				{
					unique_peak &= abs(peak_index - max_inds[j]) > clearance;	//&& max_inds[j] != 0;
				}
				if ((abs_values[peak_index] > abs_values[max_ind]) && unique_peak)
				{
					max_ind = i;
				}
			}
		}
		max_inds.push_back(max_ind);
	}
	sort(max_inds.begin(), max_inds.end());
	for (auto& ind : max_inds)
	{
		ind = sig.keys[ind];
	}

	return max_inds;
}

vector<double> CLotsOfSignalsDlg::criteria(vector<vector<int>> delays, double clear)
{
	int summ = 0;
	int s = 0;
	int buf = 0;
	vector<double> SummDelays;
	string prob = " ";
		for (int i = 0; i < delays[0].size(); i++)// �� ��������� ������ ������
	{
		summ = delays[0][i];
		s = 1;
		for (int k = 0; k < delays[0].size(); k++)//�� �������
		{
			summ += delays[s][k];
			int iter = 0;
				buf = summ;
				for (int m = 0; m < delays[0].size(); m++)// �� ��������
				{
					summ += delays[s + 1][m];
					Stroka1.push_back(to_string(summ));
					Stroka2.push_back( to_string(i) + ' ' + to_string(k) + ' ' + to_string(m));
					if (abs(summ) <= clear)
					{
						SummDelays.push_back(summ);
						string stroka1 = to_string(i) + ' ' + to_string(k) + ' ' + to_string(m);
						string stroka2 = to_string(Freq[0][i]) + ' ' + to_string(Freq[1][k]) + ' ' + to_string(Freq[2][m]);
						string stroka3 = to_string(Time[0][i]) + ' ' + to_string(Time[1][k]) + ' ' + to_string(Time[2][m]);
						str1.push_back(stroka1);
						str2.push_back(stroka2);
						str3.push_back(stroka3);
					}
					summ = buf;
				}
			summ = delays[0][i];
			
		}

	}
	return SummDelays;
}

vector<double> CLotsOfSignalsDlg::Delays(int NumberOfSources, int NumberOfSatellites)
{
	vector<double> del;
		for (int i = 0; i < NumberOfSources; i++)
	{
		for (int j = 0; j < NumberOfSatellites; j++)
		{
			del.push_back(1.+ 0.5*((double)rand() / RAND_MAX - 0.5));
			//del.push_back(DoubleRand(3, 3.1));
			//del.push_back(DoubleRand(1.5, 2));
			//del.push_back(DoubleRand(0.75, 1.25));
		}
	}

	return del;
}

double DoubleRand(double _max, double _min)
{
	return _min + double(rand()) / RAND_MAX * (_max - _min);
}

vector<double> CLotsOfSignalsDlg::DelaysFrequency(int NumberOfSources, int NumberOfSatellites)
{
	vector<double> del;
	del.clear();
	for (int i = 0; i < NumberOfSources; i++)
	{
		for (int j = 0; j < NumberOfSatellites; j++)
		{
			del.push_back(DoubleRand(-samplingFrequency / 2, samplingFrequency / 2));
		}
	}
	return del;
}

double CLotsOfSignalsDlg::DoubleRand(double _max, double _min)
{
	return _min + double(rand()) / RAND_MAX * (_max - _min);
}

void CLotsOfSignalsDlg::cutFrequency(Signal2D& insignal/*, Signal2D& outsignal*/)
{
	complex<double> buf;
	for (int i = 0; i < insignal.signal.size() - 1; i++)
	{
		for (int j = 0; j < insignal.signal[i].size()/2; j++)
		{
			buf = insignal.signal[i][j];
			insignal.signal[i][j] = insignal.signal[i][j+ insignal.signal[i].size() / 2];
			insignal.signal[i][j + insignal.signal[i].size() / 2] = buf;
		}
	}
	vector<double> bufKey;
	bufKey.resize(insignal.keysX.size());
	for (int j = 0; j < insignal.keysX.size(); j++)
	{
		if(j< insignal.keysX.size()/2)
			bufKey[j] = -insignal.keysX[-j+ insignal.keysX.size()/2];
		if (j > insignal.keysX.size() / 2)
			bufKey[j] = insignal.keysX[j - insignal.keysX.size() / 2 ];
	}
	insignal.keysX = bufKey;
	bufKey.clear();
}


vector<int> CLotsOfSignalsDlg::find_max_time(int n, vector<double>& sig, vector<double>& keysX,
vector<double>& keysY,bool time_freq, double clearance)
{
	if (sig.empty()) return {};
	vector<double> abs_values(sig.size());
	for (int i = 0; i < abs_values.size(); i++)
	{
		abs_values[i] = abs(sig[i]);
	}

	vector <int> max_inds;
	for (int max_idx = 0; max_idx < n; max_idx++)
	{
		int max_ind = 0;
		for (int i = 1; i < sig.size() - 1; i++)
		{
			if (abs_values[i - 1] < abs_values[i] && abs_values[i + 1] < abs_values[i])
			{
				int peak_index = i;
				bool unique_peak = true;
				for (int j = 0; j < max_inds.size(); j++)
				{
					unique_peak &= abs(peak_index - max_inds[j]) > clearance;	//&& max_inds[j] != 0;
				}
				if ((abs_values[peak_index] > abs_values[max_ind]) && unique_peak)
				{
					max_ind = i;
				}
			}
		}
		max_inds.push_back(max_ind);
	}
	sort(max_inds.begin(), max_inds.end());
	if (time_freq)
	{
		for (auto& ind : max_inds)
		{
			ind = keysX[ind];
		}
	}
	else
	{
		for (auto& ind : max_inds)
		{
			ind = keysY[ind];
		}
	}

	return max_inds;
}

vector<vector<int>> CLotsOfSignalsDlg::find_max_TF(int n, Signal2D& sig, double clearance)
{
	if (sig.signal.empty()) return {};
	//vector<vector<double>> abs_values(sig.signal.size());
	vector<vector<double>>* abs_values = new vector<vector<double>>(sig.signal.size());
	for (int i = 0; i < (*abs_values).size(); i++)
	{
		(*abs_values)[i].resize(sig.signal[i].size());
		for (int j = 0; j < (*abs_values)[i].size(); j++)
		{
			(*abs_values)[i][j] = abs(sig.signal[i][j]);
		}
	}

	vector<int> max_inds_freq;
	vector<int> max_inds_time;
	//max_inds.resize(n);
	for (int max_idx = 0; max_idx < n; max_idx++)
	{
		int max_ind_time = 0, max_ind_freq = 0;
		for (int i = 1; i < sig.signal.size() - 1; i++)
		{
			for (int j = 1; j < sig.signal[i].size() - 1; j++)
			{
				if (((*abs_values)[i - 1][j] < (*abs_values)[i][j] && (*abs_values)[i + 1][j] < (*abs_values)[i][j]) &&
					((*abs_values)[i][j-1] < (*abs_values)[i][j] && (*abs_values)[i][j+1] < (*abs_values)[i][j]))
				{
					int peak_indexI = i;
					int peak_indexJ = j;
					bool unique_peakI = true;
					bool unique_peakJ = true;
					for (int k = 0; k < max_inds_time.size(); k++)
					{
							unique_peakI &= abs(peak_indexI - max_inds_time[k]) > clearance;	//&& max_inds[j] != 0;
							//unique_peakJ &= abs(peak_indexJ - max_inds_freq[k]) > fr;	//???
					}
					if (((*abs_values)[peak_indexI][peak_indexJ] > (*abs_values)[max_ind_time][max_ind_freq]) && /*((*abs_values)[peak_indexJ] > (*abs_values)[max_ind_freq]) &&*/
						unique_peakI /*&& unique_peakJ*/)
					{
						max_ind_time = i;
						max_ind_freq = j;
					}
				}
			}
		}
		max_inds_time.push_back(max_ind_time);
		max_inds_freq.push_back(max_ind_freq);
	}
	//sort(max_inds.begin(), max_inds.end());
	
	timett.resize(n);
	freq.resize(n);

	Time.resize(NumberOfSatellites);
	Freq.resize(NumberOfSatellites);

	for (int indI = 0; indI < max_inds_time.size(); indI++)
	{
		timett[indI] = sig.keysY[max_inds_time[indI]];
		freq[indI] = sig.keysX[max_inds_freq[indI]];

		Time[sput].push_back(sig.keysY[max_inds_time[indI]]);
		Freq[sput].push_back(sig.keysX[max_inds_freq[indI]]);
	}
	vector<vector<int>> max_inds;
	max_inds.resize(max_inds_time.size());
	for (int indI = 0; indI < max_inds_time.size(); indI++)
	{
		max_inds[indI].push_back(timett[indI]);
		max_inds[indI].push_back(freq[indI]);
	}

	/*vector<int> buf;
	double min = Time[sput][0];
	for (int i = 0; i < Time[sput].size(); i++)
	{
		if (i > 0) min = buf[i-1];
		for (int j = i + 1; j < Time[sput].size(); j++)
		{
			if (min > Time[sput][j]) min = Time[sput][j];
		}
		buf.push_back(min);
	}
	Time[sput] = buf;*/
	sort(Time[sput].begin(), Time[sput].end());
	return max_inds;
}

vector<double> CLotsOfSignalsDlg::criteria(vector<vector<int>> delays)
{
	int summ = 0;
	int s = 0;
	int buf = 0;
	int iter = 0;
	int MinSum = 0;
	vector<double> SummDelays;
	for (int i = 0; i < delays[0].size(); i++)// �� ��������� ������ ������
	{
		summ = delays[0][i];
		s = 1;
		for (int k = 0; k < delays[0].size(); k++)//�� �������
		{
			summ += delays[s][k];
			int iter = 0;
			buf = summ;
			//vector<double>MinSum;
			for (int m = 0; m < delays[0].size(); m++)// �� ��������
			{
				summ += delays[s + 1][m];
				//MinSum.push_back(summ);
				if (abs(summ) <= clearance)
				{
					SummDelays.push_back(summ);
					string stroka = to_string(i) + ' ' + to_string(k) + ' ' + to_string(m);
					str.push_back(stroka);
					ind[iter].push_back(i);
					ind[iter].push_back(k);
					ind[iter].push_back(m);
				}
				summ = buf;
			}
			/*double min = MinSum[0];
			double m = 0;
			for (int s = 0; s < MinSum.size(); s++)
			{
				if (abs(min) > abs(MinSum[s]))
				{
					min = MinSum[s];
					m = s;
				}
			}
			SummDelays.push_back(min);
			string stroka = to_string(i) + to_string(k) + to_string(m);
			str.push_back(stroka);
			ind[iter].push_back(i);
			ind[iter].push_back(k);
			ind[iter].push_back(m);*/


			summ = delays[0][i];

		}
		iter++;
	}
	return SummDelays;
}